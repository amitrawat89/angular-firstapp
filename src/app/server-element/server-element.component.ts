import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit {

  @Output() listItemDeleted = new EventEmitter<{serverItemName: string}>();   
  @Output() selectEditItemBox = new EventEmitter<{serverItemName: string}>();
  @Output() moveListItem = new EventEmitter<{serverItemName: string}>();
  
  @Input() litem: {name: string};
  constructor() { }

  ngOnInit() {
  }

  deleteItem(value: any) {
    this.listItemDeleted.emit({serverItemName: value});
  }
 
  selectEditItem(value: any) { 
    this.selectEditItemBox.emit({serverItemName: value});
  }

  moveListItemData(value: any) {  
    this.moveListItem.emit({serverItemName: value});
  }
}
