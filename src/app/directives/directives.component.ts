import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {
  
  listData = [{name: 'Amit Rawat'}];
  listDataChild = [];
  selectedText: string='';
  message: string = '';
  // logs =[];
  i: number = 0;
  index: number = 0;
  indexValue: number=0;
  constructor() { }

  ngOnInit() {
  }

  onListItemAdded(serverData: {serverItemName: string}){ 
    this.message = '';
    this.indexValue = -1;
    
    this.indexValue = this.listData.findIndex(p => p.name == serverData.serverItemName);
    if(this.indexValue===-1) {
      this.listData.push({
        name:serverData.serverItemName
      });
    } else {
      this.message= 'Item already exists!';
    }
 }

 OnListItemDeleted(serverData: {serverItemName: string}) {
  this.message = '';
  this.indexValue = -1;   
  this.indexValue = this.listData.findIndex(p => p.name == serverData.serverItemName);
  if(this.index!==-1) {
    this.listData.splice(this.indexValue, 1);
    this.message = `Item ${serverData.serverItemName} deleted successfully`;
  }
}

OnListItemEdited(serverData: {serverItemName: string,serverNewItemName: string}) {
  this.message = '';
  // this.index = this.listData.indexOf(selvalue); 
 
   this.indexValue = -1;   
   this.indexValue = this.listData.findIndex(p => p.name == serverData.serverItemName);
   if(this.index!==-1) {
    this.listData.splice(this.indexValue, 1); 
    this.listData.push({name:serverData.serverNewItemName});    
    this.message = 'Item update successfully.';
  }  
}

OnListItemSelected(serverData: {serverItemName: string}) { 
 this.selectedText = serverData.serverItemName; 
}

 OnMoveListItem(serverData: {serverItemName: string}) { 
  
  this.message = '';
  this.indexValue = -1;   
  this.indexValue = this.listData.findIndex(p => p.name == serverData.serverItemName);
  if(this.index!==-1) {
    this.listDataChild.push({name:serverData.serverItemName}); 
    this.listData.splice(this.indexValue, 1);
    this.message = `Item ${serverData.serverItemName} moved successfully`;
  }

 }
}
