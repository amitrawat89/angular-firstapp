import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-child',
  templateUrl: './server-child.component.html',
  styleUrls: ['./server-child.component.css']
})
export class ServerChildComponent implements OnInit {

  @Input() litemchild: {name: string};
  constructor() { }

  ngOnInit() {
  }

}
