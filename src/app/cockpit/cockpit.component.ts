import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { EventEmitter } from '@angular/core/src/event_emitter';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  
  @Output() listItemAdded = new EventEmitter<{serverItemName: string}>();
  @Output() listItemEdited = new EventEmitter<{serverItemName: string,serverNewItemName: string}>();
  // selectedText: string = '';
  @Input() selectedText: string='';
  @Input() message: string='';
  constructor() { }

  ngOnInit() {
  }
  
  onSaveItem(value: any){ 
   this.listItemAdded.emit({serverItemName: value});
  }

  onEditItem(selvalue: any,value: any) {  
    this.listItemEdited.emit({
      serverItemName: selvalue,
      serverNewItemName: value
    });
  }



 clear() {
  // this.message='';
  this.selectedText='';
 }

}
